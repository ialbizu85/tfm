package tfm.uax.ion.findme;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import tfm.uax.ion.findme.nfc.NFCerUtils;
import tfm.uax.ion.findme.tfm.uax.ion.findme.services.SMSService;

public class FindMeActivity extends Activity {

    /**
     * Etiqueta para localización de LOGS correspondientes a esta clase.
     */
    private static final String TAG = "FINDME";

    private NfcAdapter mNfcAdapter;

    /**
     * Contendrá la información extraida de la etiqueta NFC
     */
    private PendingIntent pendingIntent;


    /**
     * Layout elements
     */
    private TextView txtName;
    private TextView txtTelephone;
    private TextView txtIllnessDescription;
    private ImageButton btnCall;
    private ImageButton btnLocation;
    private Button btnEmergency;

    /**
     * Location latitude
     */
    private double latitude;

    /**
     * Location longitude
     */
    private double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_me);

        txtName = (TextView) findViewById(R.id.txtName);
        txtTelephone = (TextView) findViewById(R.id.txtTelephone);
        txtIllnessDescription = (TextView) findViewById(R.id.txtIllnessDescription);
        btnLocation = (ImageButton) findViewById(R.id.btnLocation);
        btnCall = (ImageButton) findViewById(R.id.btnCall);
        btnEmergency = (Button) findViewById(R.id.btnEmergency);

        if(TextUtils.isEmpty(txtTelephone.getText().toString())){
            btnCall.setClickable(false);
            btnLocation.setClickable(false);
            btnEmergency.setClickable(false);
        }

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.",
                    Toast.LENGTH_LONG).show();
            finish();
            return;

        }

        // Rellenamos con los datos de la etiqueta leída
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        mNfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent...");

        String action = intent.getAction();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            if (intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES) != null) {

                String[] info = read(intent
                        .getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES));

                if (info.length == 5) {
                    txtName.setText(info[0]);
                    txtTelephone.setText(info[1]);
                    latitude = Double.parseDouble(info[2]);
                    longitude = Double.parseDouble(info[3]);
                    txtIllnessDescription.setText(info[4]);
                } else {
                    Toast.makeText(this,
                            getResources().getString(R.string.bad_nfc_message),
                            Toast.LENGTH_LONG).show();
                }

            }
        }

        if(!TextUtils.isEmpty(txtTelephone.getText().toString())){
            btnCall.setClickable(true);
            btnLocation.setClickable(true);
            btnEmergency.setClickable(true);

            sendFinderInformation();
        }
    }

    /**
     * Leer una etiqueta NFC
     *
     * @param rawMsgs
     */
    private String[] read(Parcelable[] rawMsgs) {
        Log.d(TAG, "Reading NFC tag ...");

        String[] info = null;

        NdefMessage[] msgs = new NdefMessage[rawMsgs.length];

        for (int i = 0; i < rawMsgs.length; i++) {
            // Por cada mensaje contenido...
            msgs[i] = (NdefMessage) rawMsgs[i];

            // ...lee todos los registros para montar el contenido completo
            NdefRecord[] records = msgs[i].getRecords();

            info = new String[records.length];
            int j = 0;

            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
                    if (Arrays
                            .equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                        try {
                            info[j] = NFCerUtils.readText(ndefRecord);
                        } catch (UnsupportedEncodingException e) {
                            Log.e(TAG, "Unsupported Encoding", e);
                        }
                    } else if (Arrays.equals(ndefRecord.getType(),
                            NdefRecord.RTD_URI)) {
                        info[j] = NFCerUtils.readURI(ndefRecord);
                    }
                }

                j++;
            }
        }
        return info;
    }


    /**
     * Send finder people information:
     * Phone number
     * Imei
     * Location
     */
    private void sendFinderInformation(){

        Intent intent = new Intent(this, SMSService.class);
        intent.putExtra("destiny", txtTelephone.getText().toString());
        intent.putExtra("name", txtName.getText().toString());
        startService(intent);
    }

    /**
     * Muestra la última ubicación conocida en google maps
     *
     * @param view
     */
    public void onClickShowLastKnowLocation(View view) {
        Uri uri = Uri.parse(String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f("
                        + getResources().getString(R.string.last_know_location) + ")",
                latitude, longitude, latitude, longitude));

        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(mapIntent);
    }


    /**
     * Llama al número de telefono indicado en la etiqueta NFC
     *
     * @param view
     */
    public void onClickCall(View view){
        Uri uri = Uri.parse("tel:" + txtTelephone.getText().toString());

        Intent intent = new Intent(Intent.ACTION_DIAL, uri);

        startActivity(intent);
    }


    /**
     * Llama a emergencias
     *
     * @param view
     */
    public void onClickCallEmergency(View view){
        String number = "112";

        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));

        startActivity(intent);
    }
}

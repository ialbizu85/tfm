package tfm.uax.ion.findme.tfm.uax.ion.findme.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SMSService extends Service implements LocationListener {

    /**
     * Latitud de la ubicación actual
     */
    private double finderLatitude;

    /**
     * Longitud de la ubicación actual
     */
    private double finderLongitude;

    /**
     * Location manager para obtener la ubicación
     */
    private LocationManager locationManager;

    /**
     * Telephony manager
     */
    private TelephonyManager telephonyManager;

    /**
     * Contact phone number
     */
    private String destiny;

    /**
     * People name
     */
    private String name;


    /**
     * Service state
     */
    private static boolean running = false;

    public SMSService() {

    }


    @Override
    public void onCreate(){
        telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        destiny = intent.getExtras().getString("destiny");
        name = intent.getExtras().getString("name");

        // Comprobamos si el proveedor GPS está disponible
       if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
           Log.e("send_sms", "detectando localización...");
           if(!running) {
               running = true;
               locationManager.requestLocationUpdates(
                       LocationManager.GPS_PROVIDER, 0, 0, this);
           }
        } else {
            if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                Log.e("send_sms", "detectando localización...");
                if(!running) {
                    running = true;
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
                }
            } else {
                if(!running) {
                    running = true;
                    //enviar SMS
                    String imei = telephonyManager.getDeviceId();
                    SmsManager smsManager = SmsManager.getDefault();
                    String message = name + " ha sido encontrado por el propietario del dispositivo con IMEI " + imei + ".";

                    Log.e("send_sms", "SMS-a kokapenik gabe bidalia");
                    smsManager.sendTextMessage(destiny, null, message, null, null);
                    running = false;
                    stopSelf();
                }
            }
        }

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy(){
        // Desactivamos el servicio una vez que hemos obtenido nuestra ubicación
        locationManager.removeUpdates(this);
    }


    public String getAddressString(Address address) {
        return (address.getThoroughfare() != null ? address.getThoroughfare() + " " : "") +
               (address.getSubThoroughfare() != null ? address.getSubThoroughfare() + " " : "") +
               (address.getLocality() != null ? address.getLocality() + " ," : "") +
               (address.getCountryName() != null ? address.getCountryName() + " " : "");
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.e("send_sms", "Kokapena aurkitua");
        //enviar SMS
        String imei = telephonyManager.getDeviceId();
        SmsManager smsManager = SmsManager.getDefault();

        String message = name + " ha sido encontrado por el propietario del dispositivo con IMEI " + imei + ".";

        if(location.getLatitude() != 0.0){
           message +=  "\nLa ubicación en la que se ha encontrado es:\n";

            //Obtener la dirección correspondiente a las coordenadas
            if (Geocoder.isPresent()) {

                List<Address> addresses = null;
                try {
                    addresses = new Geocoder(this).getFromLocation(
                            location.getLatitude(), location.getLongitude(), 1);

                    if(addresses.size() > 0){
                        Address address = addresses.get(0);
                        message += getAddressString(address);
                    }

                } catch (IOException e) {

                }
            }

            message += "\n\thttp://maps.google.com/?q=" + String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()) + "\n";
        }

        message += "\nEsta información permitirá un seguimiento de la persona en caso de que no se pongan en contacto con usted.";

        Log.e("send_sms", "SMS-a bidalita kokapenarekin");

        ArrayList<String> messageParts = smsManager.divideMessage(message);
        smsManager.sendMultipartTextMessage(destiny, null, messageParts, null, null);

        running = false;
        stopSelf();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

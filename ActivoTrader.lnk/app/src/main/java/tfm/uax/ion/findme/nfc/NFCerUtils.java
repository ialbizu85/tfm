package tfm.uax.ion.findme.nfc;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import android.nfc.NdefRecord;

public class NFCerUtils implements Ctes {

    /**
     * URI prefix
     */
    public static final String[] URI_PREFIXES = new String[] {
            "",
            "http://www.",
            "https://www.",
            "http://",
            "https://",
            "tel:",
            "mailto:",
            "ftp://anonymous:anonymous@",
            "ftp://ftp.",
            "ftps://",
            "sftp://",
            "smb://",
            "nfs://",
            "ftp://",
            "dav://",
            "news:",
            "telnet://",
            "imap:",
            "rtsp://",
            "urn:",
            "pop:",
            "sip:",
            "sips:",
            "tftp:",
            "btspp://",
            "btl2cap://",
            "btgoep://",
            "tcpobex://",
            "irdaobex://",
            "file://",
            "urn:epc:id:",
            "urn:epc:tag:",
            "urn:epc:pat:",
            "urn:epc:raw:",
            "urn:epc:",
            "urn:nfc:",
    };



    /**
     * Crea un NDEF TEXT Record para escribir en la etiqueta
     *
     * @param text
     * @return
     * @throws UnsupportedEncodingException
     */
    public static NdefRecord createTextRecord(String text) throws UnsupportedEncodingException {
        String lang = "es";
        byte[] textBytes = text.getBytes();
        byte[] langBytes = lang.getBytes();
        int textLength = textBytes.length;
        int langLength = langBytes.length;
        byte[] payload = new byte[1 + langLength + textLength];

        // Byte de estado (Ver especificaciones NDEF)
        payload[0] = (byte) langLength;

        // Copiar langbytes y textbytes en el payload
        System.arraycopy(langBytes, 0, payload, 1, langLength);
        System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

        NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload);
        return record;
    }


    /**
     * Crea un NDEF URI Record para escribir en la etiqueta
     *
     * @param text
     * @return
     * @throws UnsupportedEncodingException
     */
    public static NdefRecord createUriRecord(String text) throws UnsupportedEncodingException {
        byte[] uriBytes = text.getBytes(Charset.forName("US-ASCII"));

        // Se añade un byte para el prefijo de la URI, el primero del payload
        byte[] payload = new byte[uriBytes.length + 1];
        payload[0] = 0x01;

        // Añadimos el resto de la URI al payload
        System.arraycopy(uriBytes, 0, payload, 1, uriBytes.length);
        NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_URI, new byte[0], payload);
        return record;
    }


    /**
     * Lee el contenido de un registro NDEF de tipo RTD_TEXT
     *
     * @param record
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String readText(NdefRecord record)
            throws UnsupportedEncodingException {

        byte[] payload = record.getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
        int languageCodeLength = payload[0] & 0063;

        return new String(payload, languageCodeLength + 1, payload.length
                - languageCodeLength - 1, textEncoding);
    }

    /**
     * Lee el contenido de un registro NDEF de tipo RTD_URI
     *
     * @param record
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String readURI(NdefRecord record) {
        byte[] payload = record.getPayload();
		/*
		 * payload[0] contains the URI Identifier Code, per the NFC Forum
		 * "URI Record Type Definition" section 3.2.2.
		 *
		 * payload[1]...payload[payload.length - 1] contains the rest of the
		 * URI.
		 */
        int pre = (int) payload[0];
        String prefix = NFCerUtils.URI_PREFIXES[pre];
        String uriStr = new StringBuilder().append(prefix)
                .append(new String(payload, 1, payload.length - 1)).toString();

        return uriStr;
    }
}

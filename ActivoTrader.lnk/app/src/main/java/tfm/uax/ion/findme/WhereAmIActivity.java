package tfm.uax.ion.findme;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import tfm.uax.ion.findme.nfc.NFCerUtils;

public class WhereAmIActivity extends Activity implements LocationListener {

    /**
     * Etiqueta para los logs
     */
    private static final String TAG = "WHERE_AM_I";

    /**
     * Adapter para NFC
     */
    private NfcAdapter mNfcAdapter;

    /**
     * Contendrá la información extraida de la etiqueta NFC
     */
    private PendingIntent pendingIntent;

    /**
     * Indica si está escribiendo en una etiqueta NFC
     */
    boolean writeMode;

    /**
     * Etiqueta NFC
     */
    private Tag tag;

    /**
     * Progress Dialog para cuando se esta detectando la ubicación actual
     */
    private ProgressDialog progressDialog;

    /**
     * Location manager para obtener la ubicación
     */
    private LocationManager locationManager;

    /**
     * Latitud de la ubicación actual
     */
    private double latitude;

    /**
     * Longitud de la ubicación actual
     */
    private double longitude;

    /**
     * Layout elements
     */
    private TextView txtLatitude;
    private TextView txtLongitude;
    private EditText editName;
    private EditText editTelephone;
    private EditText editIllness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_where_am_i);

        txtLatitude = (TextView) findViewById(R.id.txtLatitude);
        txtLongitude = (TextView) findViewById(R.id.txtLongitude);

        editName = (EditText) findViewById(R.id.editName);
        editTelephone = (EditText) findViewById(R.id.editTelephone);
        editIllness = (EditText) findViewById(R.id.editIllness);

        // Comprobar que el NFCAdapter está disponible
        mNfcAdapter = NfcAdapter.getDefaultAdapter(WhereAmIActivity.this);
        if (mNfcAdapter == null) {
            Toast.makeText(WhereAmIActivity.this,
                    getResources().getString(R.string.no_nfc),
                    Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    public void onClickGetCurrentLocation(View view) {

        locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        // Comprobamos si el proveedor GPS está disponible
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            // Crear el cuadro de diálogo para activar GPS
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.enable_gps));
            builder.setPositiveButton(
                    getResources().getString(R.string.active_gps),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Abrimos la página de configuración del GPS
                            startActivityForResult(new Intent(
                                            Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                    0);
                        }
                    });

            builder.setNegativeButton(
                    getResources().getString(R.string.cancel),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Cerramos el cuadro de diálogo
                            dialog.cancel();
                        }
                    });

            AlertDialog alert = builder.create();
            alert.show();
        } else {

            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0, this);
            progressDialog = ProgressDialog.show(this, "", getResources()
                    .getString(R.string.getting_location));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Comprobamos si el proveedor GPS está disponible después de mostrar el
        // cuadro de diálogo
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this,
                    getResources().getString(R.string.provider_error_msg),
                    Toast.LENGTH_LONG).show();
        } else {
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 0, 0, this);
            progressDialog = ProgressDialog.show(this, "", getResources()
                    .getString(R.string.getting_location));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
    }

    @Override
    public void onNewIntent(Intent intent) {
        tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
    }

    public void onClickWriteTag(View view) {

        if (validInformation()) {
            try {
                String[] info = { editName.getText().toString(),
                        editTelephone.getText().toString(),
                        String.valueOf(latitude), String.valueOf(longitude), editIllness.getText().toString() };

                if (write(info, NFCerUtils.RTD_TEXT)) {
                    Toast.makeText(this,
                            getResources().getString(R.string.write_success_msg),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(
                            this,
                            getResources()
                                    .getString(R.string.write_error_msg),
                            Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                Log.e(TAG, "I/O Exception");
            } catch (FormatException e) {
                Log.e(TAG, "Format Exception");
            }
        } else {
            Toast.makeText(this, getString(R.string.write_tag_validation),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private boolean write(String[] info, String writtingType)
            throws IOException, FormatException {
        boolean result = false;

        Log.d(TAG, "Writting NFC tag ...");

        if (tag != null) {
            Log.d(TAG, "Tag encontrado ...");
            NdefRecord[] records = new NdefRecord[info.length];

            for(int i = 0; i < info.length; i++){
                if (writtingType.equals(NFCerUtils.RTD_TEXT)) {
                    records[i] = NFCerUtils.createTextRecord(info[i]);
                }

                if (writtingType.equals(NFCerUtils.RTD_URI)) {
                    records[i] = NFCerUtils.createUriRecord(info[i]);
                }
            }

            NdefMessage message = new NdefMessage(records);

            // Obtener una instancia de Ndef para la etiqueta
            Ndef ndef = Ndef.get(tag);

            // Activar I/O
            ndef.connect();

            // Escribir el mensaje
            ndef.writeNdefMessage(message);

            // Cerrar la conexión
            ndef.close();

            result = true;
        }

        return result;
    }


    /**
     * Comprobamos que se haya introducido toda la información
     * @return boolean
     */
    private boolean validInformation() {
        Log.d(TAG, "Validate information...");

        if(TextUtils.isEmpty(editName.getText())){
            Log.d(TAG, "No name...");
            return false;
        }

        if(TextUtils.isEmpty(editTelephone.getText())){
            Log.d(TAG, "No telephone...");
            return false;
        }

        if(latitude == Double.NaN){
            Log.d(TAG, "No location...");
            return false;
        }

        if(longitude == Double.NaN){
            Log.d(TAG, "No location...");
            return false;
        }

        Log.d(TAG, "Validación satisfactoria...");
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {

        latitude = location.getLatitude();
        longitude = location.getLongitude();

        txtLatitude.setText("Lat: " + latitude);
        txtLongitude.setText("lon: " + longitude);

        // Desactivamos el servicio una vez que hemos obtenido nuestra ubicación
        locationManager.removeUpdates(this);

        progressDialog.dismiss();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }
}